package edu.uchicago.gerber.labweather2019;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import edu.uchicago.gerber.labweather2019.jigs.Response;
import edu.uchicago.gerber.labweather2019.jigs.Temp;



public class MainActivity extends AppCompatActivity {



    //use this string to get the sample json
    //https://api.openweathermap.org/data/2.5/forecast/daily?q=boston&units=imperial&cnt=16&APPID=acb056481e1dddbd0324f1ca9d349f4f

    //use this url to generate the jigs, and use Response for classname, and the full package to your jigs: e.g. edu.uchicago.gerber.labweatherredux.jigs for package.
    //http://www.jsonschema2pojo.org/

    //package is the fully-qualified package to the jigs directory
    //Class name is: Response
    //Target language is: Java
    //Source type: JSON
    //Annotation style: Gson
    //accept all other defaults
    //zip and unzip to the jigs directory



    // List of Weather objects representing the forecast
    private List<Weather> weatherList = new ArrayList<>();

    private ImageView imageCity;

    // ArrayAdapter for binding Weather objects to a ListView
    private WeatherArrayAdapter weatherArrayAdapter;
    private ListView weatherListView; // displays weather info

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageCity = findViewById(R.id.imageCity);


        // create ArrayAdapter to bind weatherList to the weatherListView
        weatherListView = (ListView) findViewById(R.id.weatherListView);
        weatherArrayAdapter = new WeatherArrayAdapter(this, weatherList);
        weatherListView.setAdapter(weatherArrayAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                EditText locationEditText =
                        (EditText) findViewById(R.id.locationEditText);
                URL url = createURL(locationEditText.getText().toString());

                // hide keyboard and initiate a GetWeatherTask to download
                // weather data from OpenWeatherMap.org in a separate thread
                if (url != null) {
                    dismissKeyboard(locationEditText);
                     GetWeatherTask getLocalWeatherTask = new GetWeatherTask();
                      getLocalWeatherTask.execute(url);
                }
                else {

                    //this may need to be in a catch block
                    Snackbar.make(view, R.string.invalid_url, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }



            }
        });
    }


    // programmatically dismiss keyboard when user touches FAB
    private void dismissKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // create openweathermap.org web service URL using city
    private URL createURL(String city) {
        String apiKey = getString(R.string.api_key);
        String baseUrl = getString(R.string.web_service_url);

        try {
            // create URL for specified city and imperial units (Fahrenheit)
            String urlString = baseUrl + URLEncoder.encode(city, "UTF-8") +
                    "&units=imperial&cnt=16&APPID=" + apiKey;
            return new URL(urlString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null; // URL was malformed
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    // makes the REST web service call to get weather data and
    // saves the data to a local HTML file
    private class GetWeatherTask
            extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {


            JSONObject object= null;

            try {
                object =  Util.getJSON(params[0].toString(), 2000);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }


        // process JSON response and update ListView
        @Override
        protected void onPostExecute(JSONObject weather) {
           Response response =   convertJSONtoArrayList(weather); // repopulate weatherList



            weatherArrayAdapter.notifyDataSetChanged(); // rebind to ListView
            weatherListView.smoothScrollToPosition(0); // scroll to top


            //No longer available w/o api key
//            Glide.with(MainActivity.this).load("http://maps.google.com/maps/api/staticmap?center=" + response.getCity().getCoord().getLat()+ "," + response.getCity().getCoord().getLon() +
//                    "&zoom=10&size=800x800&sensor=false").


            //you can use the data in the reponse to tweak the url, here are the instructions -> //https://openweathermap.org/api/weathermaps#urlformat
            Log.d("WEATHER", response.toString());


                  Glide.with(MainActivity.this)
                    .load("https://tile.openweathermap.org/map/precipitation_new/3/2/2.png?appid=" + getString(R.string.api_key))
//                 .placeholder(R.color.photo_placeholder)
//                 .error(R.color.photo_placeholder_error)
                    .into(imageCity);

        }
    }


    // create Weather objects from JSONObject containing the forecast
    private Response convertJSONtoArrayList(JSONObject forecast) {
        weatherList.clear(); // clear old weather data


        Gson gson = new Gson();

        Response response =  gson.fromJson(forecast.toString(), Response.class);

        Log.d("RESP", response.toString());

        List<edu.uchicago.gerber.labweather2019.jigs.List> list = response.getList();

        for (int i = 0; i <list.size(); i++) {
            edu.uchicago.gerber.labweather2019.jigs.List day =    list.get(i);
            Temp temp = day.getTemp();
            List<edu.uchicago.gerber.labweather2019.jigs.Weather> weathers = day.getWeather();
            edu.uchicago.gerber.labweather2019.jigs.Weather weather = weathers.get(0);

            weatherList.add(new Weather(


                    day.getDt(), // date/time timestamp
                    temp.getMin(), // minimum temperature
                    temp.getMax(), // maximum temperature
                    day.getHumidity(), // percent humidity
                    weather.getDescription(), // weather conditions
                    weather.getIcon()

            ));



        }

          return response;

    }
}
